package main

// Copyright 2015 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

var addr1 *string //= flag.String("addr", "internal-ws-server-asg-attempt2-1-1224977945.ap-southeast-1.elb.amazonaws.com:8080", "http service address")
var clientCount int64
var countChan chan int64

func main() {
	flag.Parse()
	log.SetFlags(0)
	rand.Seed(time.Now().UnixNano())
	setLimit()
	clientCount = 0
	var serverName, ok = os.LookupEnv("SERVER_HOST")
	if !ok {
		serverName = "127.0.0.1"
	}
	addr1 = flag.String("addr", serverName+ ":80", "http service address")
	countChan = make(chan int64, 1000)
	go countUpdater()

	r := mux.NewRouter()
	r.HandleFunc("/add/{connections:[0-9]+}/{durationMs:[0-9]+}", addConn)
	r.HandleFunc("/", countHandler)

	http.ListenAndServe(":8081", r)
}


func addConn(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	connections := int64(0)
	d := 10
	connections, _ = strconv.ParseInt(vars["connections"], 10, 64)
	d , _ = strconv.Atoi(vars["durationMs"])

	go addConnections(connections, time.Duration(d) * time.Millisecond)

	w.Write([]byte(fmt.Sprintf("connections: %v, duration : %v\n", connections, d)))
}

func countHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(fmt.Sprintf("Active connections (from client): %d\n", clientCount)))
}

func addConnections(totalConnections int64, intervalMs time.Duration) {
	var wg sync.WaitGroup
	i := clientCount
	for ; i < totalConnections; i++ {
		time.Sleep(intervalMs)
		wg.Add(1)
		countChan <- 1
		go func() {
			defer wg.Done()
			defer func() {
				countChan <- -1
			}()
			client()
		}()
	}
	wg.Wait()
}

func countUpdater() {
	for {
		clientCount += <-countChan
	}
}

func setLimit() {
	var rLimit syscall.Rlimit
	err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit)
	if err != nil {
		fmt.Println("Error Getting Rlimit ", err)
	}
	fmt.Println(rLimit)
	rLimit.Cur = rLimit.Max
	err = syscall.Setrlimit(syscall.RLIMIT_NOFILE, &rLimit)
	if err != nil {
		fmt.Println("Error Setting Rlimit ", err)
	}
	err = syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit)
	if err != nil {
		fmt.Println("Error Getting Rlimit ", err)
	}
	fmt.Println("Rlimit Final", rLimit)
}

func client() {
	u := url.URL{Scheme: "ws", Host: *addr1, Path: "/echo"}
	log.Printf("connecting to %s", u.String())

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Println("error dialing:", err)
		return
	}
	defer c.Close()

	done := make(chan struct{})

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}
			log.Printf("recv: %s", message)
		}
	}()

	c.SetPingHandler(func(appData string) error {
		c.WriteMessage(websocket.PongMessage,nil)
		log.Print("ping.")
		return nil
	})
	for {
		select {
		case <-done:
			return
		case <-interrupt:
			log.Println("interrupt")

			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		}
	}
}
